## TOOLS:

- Tools for measurement, design, testing, deploying, communicating and sharing are essential and more vital when it comes to GIS. For any application or domain that 
- likely to use Geo-Information has to use the required tool that keep the systems integrated together. The following will the updatable table which has to be 
- FOSS based - as a catalogue of tools available designed with open standards and philosophy in mind. 

Please do fill the unfilled fields and records. :)

#### GEOGRAPHICAL INFORMATION SYSTEMS : (Geomatics / Surveying / Mapping / Cartography / Remote Sensing / Transit)

| No. |	Name		|	Type		|	Based on	|	Purpose		|	License		|	Personal Experience	|
| - |:----------------------|:----------------------|:----------------------|:----------------------|:---------------------:|:------------------------------|
| 1. | GDAL,OGR,GEOS		| Platform - GIS	|			| Base for GIS		|			|				|
| 2. | GRASS			| GIS			|			| GIS Desktop Tool	|			|	...			|
| 3. | QGIS			| GIS			|			| GIS Desktop Tool	|			|	....			|
| 4. | SAGA			| GIS			|			| GIS Desktop Tool	|			|				|
| 5. | TauDEM		| GIS			|			| GIS Desktop Tool	|			|				|
| 6. | Udig			| GIS			|			| GIS Desktop Tool	|			|	.			|
| 7. | gvSIG         | GIS           |           | GIS Desktop Tool  |           |               |
| 8. | OpenJump      | GIS           |           | GIS Desktop Tool  |           |               |
| 9. | Orfeo Toolbox | Sat, Image Processing |   | Sat. Image Processing |       |               |
| 10. | GeoDa, GeoDaspace | Geo Spatial Explore |  | Data Exploration |           |               |
| 11. | STEP          | Science Toolbox Exploitation Platform |       |  Data Sourcing Toolbox |          |       |
| 12. | RSToolbox          | RS |       |  Remote sensing for R |          ||
| 13. | Pyramid Shader	| GIS			|			| GIS Desktop Tool	|			|	.			|
| 14. | Terrain Equalizer	| GIS			|			| GIS Desktop Tool	|			|	.			|
| 15. | Terrain Bender	| GIS			|			| GIS Desktop Tool	|			|	.			|
| 16. | Scree Painter		| GIS			|			| GIS Desktop Tool	|			|	.			|
| 17. | Tangible Landscape | GIS          |           | Terrain Modeling & Interaction   |        |           |
| 18. | WhiteBox GAT      | Geospatial Analysis |     | GIS Desktop Tool  |           |               |
| 19. | JOSM			| GIS - Collaborate	|			| Collabore, Contribute	|			|	....			|
| 20. | Maputnik		| GIS - Carto design	|	react, js	| Design Tool		|			|				|
| 21. | Mapnik		| GIS - Web		|			| Web GIS		|			|				|
| 22. | uMap			| GIS - Web		|			| Web GIS		|			|				|
| 23. | Tilemill/Tileoven		| GIS - Web		|			| Web GIS		|			|				|
| 24. | GeoServer		| GIS - Web		|			| Web GIS		|			|				|
| 25. | GeoNode		| GIS - Web		|			| Web GIS		|			|				|
| 26. | OpenLayers		| GIS - Web		|			| Web GIS		|			|				|
| 27. | Leaflet		| GIS - Web		|			| Web GIS		|			|				|
| 28. | Turf			| GIS - Web		|			| Web GIS		|			|				|
| 29. | GeoJS			| GIS - Web		|			| Web GIS		|			|				|
| 30. | Tippecanoe		| GIS - Tile generator	|			| GIS-CLI		|			|				|
| 31. | Rasterio		| GIS - Raster		|			| GIS-CLI		|			|				|
| 32. | Tileserver		| GIS - Local Serve	|			| Web GIS		|			|	..			|
| 33. | Mapserver		| GIS - WMS/WFS		| 	C		| Web GIS		|			|				|
| 34. | Cesium		| GIS - 3D		|			| Web GIS		|			|				|
| 35. | OSM-P2P		| GIS - P2P		|			| P2P GIS		|			|				|
| 36. | OpenForis     | Collect       |           | Data Collection (Files) |   |               |
| 37. | OpenForis     | Collect Mobile |          | Data Collection (Mobile) |   |               |
| 38. | OpenForis     | Collect Earth  |           | Data Collection (Sats.) |   |               |
| 39. | OpenForis     | Calc          |           | Data Analysis |           |               |
| 40. | OpenForis     | Geospatial Toolkit |      |               |           |               |
| 41. | PostGIS		| GIS - DB		|			| Storage GIS (DBMS)	|			|				|
| 42. | SpatialLite		| GIS - DB		|			| Storage GIS (DBMS)	|			|				|
| 43. | GeoJSON,TopoJSON	| GIS - DataType	|			| Serializing GIS data	|			|				|
| 44. | TransitWand		| GIS - Transit		|			| Preprocessing GTFS	|			|				|
| 45. | Transitfeed		| GIS - Transit		|			| GTFS feed Validataion |			|				|
| 46. | GTFS - Editor		| GIS - Transit		|			| GTFS Editor		|			|				|
| 47. | OpenTripPlanner	| GIS - Transit		|			| Trip plan & mgmt	|			|				|
| 48. | Analyst server	| GIS - Transit		|			| Transit analysis	|			|				|
| 49. | GTFS - rt admin	| GIS - Transit		|			| GTFS RT admin		|			|				|
| 50. | Node-GTFS		| GIS - Transit		|			| Node based GTFS tool	|			|				|
| 51. | GTFSdb		| GIS - Transit		|			| GTFS database		|			|				|
| 52. | GTFS to HTML		| GIS - Transit		|			| Time table generation |			|				|
| 53. | Traffic-Engine	| GIS - Transit		|			| Traffic statistics	|			|				|
| 54. | GO Sync		| GTFS - OSM Sync	|			|      	     		|			|				|
| 55. | GPX Filter		| GPX Visualization	|			| 	  		|			|				|
| 56. | GPX Map Matcher	| GPX Visualization	|			|			|			|				|
| 57. | ulogger Server	| Location Tracking	|			|			|			|				|
| 58. | TracCar	| Location Tracking	|			|			|			|				|
| 59. | OwnTracks	| Location Tracking	|			|			|			|				|
| 60. | Geomesa	| GIS - Large Scale	|			| Querying & Analytics		|			|				|
| 61. | GeoWave	| GIS - Large Scale	|			| Querying & Analytics		|			|				|
| 62. | POSM	| Portable OSM	|			| Self Reliant, Offline		|			|				|